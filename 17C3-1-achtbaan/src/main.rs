use std::mem::replace;

fn main() {
    let input = get_input();

    let tracks = input
        .lines()
        .skip(1)
        .map(|line| line.split_whitespace().skip(1).next().unwrap())
        .map(|track_line| track_line.chars().map(TrackPart::from_char));

    for (index, track) in tracks.enumerate() {
        let mut map = Map::default();
        let mut cx = 256;
        let mut cy = 256;
        let mut cz = 256;
        let mut dx = 1_i16;
        let mut dz = 0_i16;

        for part in track {
            debug_assert!(cz > 0);

            match part {
                TrackPart::Station => map.insert(cx, cy, cz, '='),
                TrackPart::Straight => map.insert(cx, cy, cz, '_'),
                TrackPart::Left => {
                    map.insert(cx, cy, cz, '_');
                    dx = replace(&mut dz, -dx);
                }
                TrackPart::Right => {
                    map.insert(cx, cy, cz, '_');
                    dz = replace(&mut dx, -dz);
                }
                TrackPart::Up => {
                    if dx > 0 {
                        map.insert(cx, cy, cz, '/');
                    } else if dx < 0 {
                        map.insert(cx, cy, cz, '\\');
                    } else {
                        map.insert(cx, cy, cz, '#');
                    }
                    cy -= 1;
                }
                TrackPart::Down => {
                    cy += 1;
                    if dx > 0 {
                        map.insert(cx, cy, cz, '\\');
                    } else if dx < 0 {
                        map.insert(cx, cy, cz, '/');
                    } else {
                        map.insert(cx, cy, cz, '#');
                    }
                }
            }

            cx += dx;
            cz += dz;
        }

        for y in map.min_y..(map.max_y + 1) {
            print!("{} ", index + 1);
            for x in map.min_x..(map.max_x + 1) {
                print!("{}", map.cells[y as usize][x as usize].0);
            }
            println!();
        }
    }
}

fn get_input() -> String {
    use std::io::{stdin, Read};

    let mut buffer = String::new();
    stdin().read_to_string(&mut buffer).unwrap();
    buffer
}

pub struct Map {
    min_x: i16,
    max_x: i16,
    min_y: i16,
    max_y: i16,
    cells: [[(char, i16); 512]; 512],
}

impl Default for Map {
    fn default() -> Map {
        Map {
            min_x: 512,
            max_x: -1,
            min_y: 512,
            max_y: -1,
            cells: [[('.', -1); 512]; 512],
        }
    }
}

impl Map {
    fn insert(&mut self, x: i16, y: i16, z: i16, ch: char) {
        use std::cmp::{max, min};
        let (_, depth) = self.cells[y as usize][x as usize];

        if depth < z {
            self.cells[y as usize][x as usize] = (ch, z);
            self.min_x = min(self.min_x, x);
            self.max_x = max(self.max_x, x);
            self.min_y = min(self.min_y, y);
            self.max_y = max(self.max_y, y);
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
enum TrackPart {
    Station,
    Straight,
    Up,
    Down,
    Left,
    Right,
}

impl TrackPart {
    fn from_char(ch: char) -> TrackPart {
        match ch {
            'S' => TrackPart::Station,
            'V' => TrackPart::Straight,
            'U' => TrackPart::Up,
            'D' => TrackPart::Down,
            'L' => TrackPart::Left,
            'R' => TrackPart::Right,
            _ => panic!(),
        }
    }
}
