fn main() {
    let input = get_input();
    let lines = &mut input.lines();
    let challange_count: usize = lines.next().unwrap().parse().unwrap();

    for challange_index in 1..(challange_count + 1) {
        let winning_numbers = Results::from_str(lines.next().unwrap());

        let tries: usize = lines.next().unwrap().parse().unwrap();

        let money: usize = lines
            .take(tries)
            .map(parse_gamble)
            .map(|gamble| winning_numbers.get_money(&gamble))
            .sum();
        println!("{} {}", challange_index, money);
    }
}

#[derive(Default)]
struct Results {
    numbers: [u8; 6],
    reserve: u8,
}

impl Results {
    fn from_str(line: &str) -> Self {
        let mut number_line = Results::default();
        let mut numbers = line.split_whitespace();

        for (index, num_str) in (&mut numbers).take(6).enumerate() {
            number_line.numbers[index] = num_str.parse().unwrap();
        }

        number_line.reserve = numbers.next().unwrap().parse().unwrap();
        number_line
    }

    fn get_money(&self, gamble: &[u8; 6]) -> usize {
        let matches = self.numbers
            .iter()
            .cloned()
            .filter(|num| gamble.binary_search(num).is_ok())
            .count();
        let reserve_match = gamble.binary_search(&self.reserve).is_ok();

        match (matches, reserve_match) {
            (2, true) => 3,
            (3, false) => 5,
            (3, true) => 8,
            (4, false) => 22,
            (4, true) => 218,
            (5, false) => 1201,
            (5, true) => 35722,
            (6, _) => 1000000,
            _ => 0,
        }
    }
}

fn get_input() -> String {
    use std::io::Read;
    use std::io::stdin;

    let mut buf = String::new();
    stdin().read_to_string(&mut buf).unwrap();
    buf
}

fn parse_gamble(line: &str) -> [u8; 6] {
    let mut results = [0; 6];

    for (index, num_str) in line.split_whitespace().enumerate() {
        results[index] = num_str.parse().unwrap();
    }

    results
}
