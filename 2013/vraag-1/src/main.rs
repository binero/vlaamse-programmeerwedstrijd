use std::io::BufRead;

#[derive(Eq, PartialEq, Clone, Copy)]
enum State {
    Busy,
    Free,
}

impl From<char> for State {
    fn from(input: char) -> State {
        match input {
            'B' => State::Busy,
            'V' => State::Free,
            _ => panic!("tewtsetsete"),
        }
    }
}

fn intersect(a: Vec<State>, b: Vec<State>) -> Vec<State> {
    a.into_iter()
        .zip(b.into_iter())
        .map(|(a, b)| {
            if a == State::Busy || b == State::Busy {
                State::Busy
            } else {
                State::Free
            }
        })
        .collect()
}

fn call(index: usize, input_iter: &mut impl Iterator<Item = String>) -> Result<(), Box<Error>> {
    let week_length: usize = input_iter.next().unwrap().parse()?;
    let intersection_length: usize = input_iter.next().unwrap().parse()?;

    let calendars =
        input_iter.map(|string| string.chars().map(State::from).collect::<Vec<State>>());

    let intersection = calendars.fold(vec![State::Free; week_length], intersect);
    if let Some((index, _)) = intersection
        .windows(intersection_length)
        .enumerate()
        .find(|&(_, window)| window.iter().find(|&&state| state == State::Busy).is_none())
    {
        println!("{}", index + 1);
    } else {
        println!("X");
    }
    Ok(())
}

// Don't look down

use std::error::Error;
fn main() -> Result<(), Box<Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int(&mut input).expect("No ex. count");

    for excercise_index in 1..=excercise_count {
        let line_count: usize =
            2 + read_int(&mut input).expect(&format!("No line count for {}", excercise_count));

        let mut excercise_iter = (&mut input).take(line_count);
        call(excercise_index, &mut excercise_iter)
            .unwrap_or_else(|err| eprintln!("Ex {} failed: {}", excercise_index, err));

        {
            let count = excercise_iter.count();
            if count > 0 {
                eprintln!("Test case {} failed: {} lines left", excercise_index, count);
            }
        }
    }
    Ok(())
}

fn read_int(input: &mut impl Iterator<Item = String>) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
