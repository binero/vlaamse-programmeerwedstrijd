use std::io::BufRead;

fn get_value(a: &[usize], b: &[usize]) -> usize {
    a[0].max(b[0]) + a[1].max(b[1])
}

fn call(index: usize, input_iter: &mut impl Iterator<Item = String>) -> Result<(), Box<Error>> {
    let paths: Vec<Vec<usize>> = input_iter
        .map(|string| {
            string
                .split_whitespace()
                .map(str::parse)
                .map(Result::unwrap)
                .collect()
        })
        .collect();

    let result: usize = (0..paths.len())
        .filter_map(|a_index| {
            ((a_index + 1)..paths.len())
                .map(|b_index| get_value(&paths[a_index], &paths[b_index]))
                .min()
        })
        .min()
        .unwrap();

    println!("{}", result);

    Ok(())
}

// Don't look down

use std::error::Error;
fn main() -> Result<(), Box<Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int(&mut input).expect("No ex. count");

    for excercise_index in 1..=excercise_count {
        let line_count: usize =
            read_int(&mut input).expect(&format!("No line count for {}", excercise_count));

        let mut excercise_iter = (&mut input).take(line_count);
        call(excercise_index, &mut excercise_iter)
            .unwrap_or_else(|err| eprintln!("Ex {} failed: {}", excercise_index, err));

        {
            let count = excercise_iter.count();
            if count > 0 {
                eprintln!("Test case {} failed: {} lines left", excercise_index, count);
            }
        }
    }
    Ok(())
}

fn read_int(input: &mut impl Iterator<Item = String>) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
