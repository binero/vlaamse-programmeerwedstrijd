use std::io::BufRead;

#[derive(Eq, Clone, PartialEq, Ord, PartialOrd, Debug)]
struct Puzzle {
    inner: Vec<Vec<char>>,
}

impl Puzzle {
    fn read(string: String, width: usize, height: usize) -> Puzzle {
        let mut input = string.chars();
        let mut inner = Vec::new();

        for x in 0..height {
            inner.push((&mut input).take(width).collect());
        }
        return Puzzle { inner };
    }

    fn width(&self) -> isize {
        self.inner[0].len() as isize
    }
    fn height(&self) -> isize {
        self.inner.len() as isize
    }
    fn at(&self, x: isize, y: isize) -> char {
        self.inner[y as usize][x as usize]
    }
    fn coords(&self) -> (isize, isize) {
        (0..self.height())
            .flat_map(|y| (0..self.width()).map(move |x| (x, y)))
            .find(|&(x, y)| self.at(x, y) == ' ')
            .unwrap()
    }
    fn swap(&mut self, (x1, y1): (isize, isize), (x2, y2): (isize, isize)) {
        let lol = self.inner[y1 as usize][x1 as usize];
        self.inner[y1 as usize][x1 as usize] = self.inner[y2 as usize][x2 as usize];
        self.inner[y2 as usize][x2 as usize] = lol;
    }

    fn expand(self) -> Vec<Puzzle> {
        let width = self.width();
        let height = self.height();

        const DIRECTIONS: [(isize, isize); 4] = [(-1, 0), (1, 0), (0, -1), (0, 1)];
        let (x, y) = self.coords();
        DIRECTIONS
            .iter()
            .cloned()
            .map(|(rel_x, rel_y)| (x + rel_x, y + rel_y))
            .filter(|&(to_x, to_y)| to_x >= 0 && to_y >= 0 && to_x < width && to_y < height)
            .map(|(to_x, to_y)| {
                let mut new = self.clone();
                new.swap((x, y), (to_x, to_y));
                new
            })
            .collect()
    }
}

fn solution_found(starts: &mut [Puzzle], ends: &[Puzzle]) -> bool {
    starts.sort();
    for end in ends.iter() {
        for start in starts.iter() {
            if end == start {
                return true;
            }
        }
    }
    return false;
}

fn call(index: usize, input_iter: &mut impl Iterator<Item = String>) -> Result<(), Box<Error>> {
    const MAX_STEPS: usize = 7;

    let numbers_lol = input_iter.next().unwrap();
    let mut numbers = numbers_lol.split_whitespace();
    let height = numbers.next().unwrap().parse().unwrap();
    let width = numbers.next().unwrap().parse().unwrap();

    let start = Puzzle::read(input_iter.next().unwrap(), width, height);
    let end = Puzzle::read(input_iter.next().unwrap(), width, height);

    let mut starts = vec![start];
    let mut ends = vec![end];

    if solution_found(&mut starts, &ends) {
        println!("{}", 0);
        return Ok(());
    }

    for steps_taken in 1..=MAX_STEPS {
        starts = starts
            .into_iter()
            .flat_map(|puzzle| puzzle.expand())
            .collect();
        if solution_found(&mut starts, &ends) {
            println!("{}", steps_taken);
            return Ok(());
        }
    }

    for steps_taken in 1..=MAX_STEPS {
        ends = ends
            .into_iter()
            .flat_map(|puzzle| puzzle.expand())
            .collect();
        if solution_found(&mut starts, &ends) {
            println!("{}", MAX_STEPS + steps_taken);
            return Ok(());
        }
    }

    Ok(())
}

// Don't look down

use std::error::Error;
fn main() -> Result<(), Box<Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int(&mut input).expect("No ex. count");

    for excercise_index in 1..=excercise_count {
        call(excercise_index, &mut input)
            .unwrap_or_else(|err| eprintln!("Ex {} failed: {}", excercise_index, err));
    }
    Ok(())
}

fn read_int(input: &mut impl Iterator<Item = String>) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
