use std::io::BufRead;

fn is_possible(valid_stores: &[Vec<char>], order: &[char]) -> bool {
    if let Some(current_order) = order.first() {
        let backlog = &order[1..];
        let result = valid_stores.iter().enumerate().find(|&(index, store)| {
            if !store.contains(current_order) {
                return false;
            } else {
                let bussiness: Vec<Vec<char>> = valid_stores
                    .iter()
                    .enumerate()
                    .filter(|&(cindex, _)| cindex != index)
                    .map(|(_, store)| store)
                    .cloned()
                    .collect();

                return is_possible(&bussiness, backlog);
            }
        });

        return result.is_some();
    } else {
        return true;
    };
}

fn call(index: usize, input_iter: &mut impl Iterator<Item = String>) -> Result<(), Box<Error>> {
    let store_size: usize = input_iter.next().unwrap().parse()?;
    let store: Vec<Vec<char>> = input_iter
        .take(store_size)
        .map(|string| string.chars().collect())
        .collect();

    let customer_count: usize = input_iter.next().unwrap().parse()?;
    let customers: Vec<Vec<char>> = input_iter
        .take(customer_count)
        .map(|string| string.chars().collect())
        .collect();

    'lol: for customer in customers {
        let store_string = store
            .iter()
            .flat_map(|store| store)
            .cloned()
            .collect::<Vec<char>>();

        for cher in &customer {
            if !store_string.contains(&cher) {
                println!("{} onmogelijk", customer.iter().collect::<String>());
                continue 'lol;
            }
        }
        if is_possible(&store, &customer) {
            println!("{} mogelijk", customer.iter().collect::<String>());
        } else {
            println!("{} onmogelijk", customer.iter().collect::<String>());
        }
    }

    Ok(())
}

// Don't look down

use std::error::Error;
fn main() -> Result<(), Box<Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int(&mut input).expect("No ex. count");

    for excercise_index in 1..=excercise_count {
        call(excercise_index, &mut input)
            .unwrap_or_else(|err| eprintln!("Ex {} failed: {}", excercise_index, err));
    }
    Ok(())
}

fn read_int(input: &mut impl Iterator<Item = String>) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
