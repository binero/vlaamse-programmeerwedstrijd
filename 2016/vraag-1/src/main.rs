fn main() {
    let input = get_input();
    let lines = &mut input.lines();
    let challange_count: usize = lines.next().unwrap().parse().unwrap();

    for id in 0..challange_count {
        let budgets: Vec<u32> = lines
            .next()
            .unwrap()
            .split_whitespace()
            .skip(1)
            .map(|x| x.parse().unwrap())
            .collect();

        let food_truck_count: u32 = lines.next().unwrap().parse().unwrap();
        let food_trucks: Vec<Vec<u32>> = (0..food_truck_count)
            .map(|_| {
                lines
                    .next()
                    .unwrap()
                    .split_whitespace()
                    .skip(1)
                    .map(|price| price.parse().unwrap())
                    .collect()
            })
            .collect();

        let working: Vec<u32> = budgets
            .iter()
            .cloned()
            .filter(|&budget| can_resolve(budget, &*food_trucks))
            .collect();
        print!("{}", id + 1);
        if working.len() == 0 {
            println!(" GEEN");
        } else {
            for works in working {
                print!(" {}", works);
            }
            println!();
        }
    }
}

fn get_input() -> String {
    use std::io::Read;
    use std::io::stdin;

    let mut buf = String::new();
    stdin().read_to_string(&mut buf).unwrap();
    buf
}

fn can_resolve(budget: u32, food_trucks: &[Vec<u32>]) -> bool {
    if budget == 0 && food_trucks.len() == 0 {
        true
    } else if food_trucks.len() == 0 {
        false
    } else {
        food_trucks[0]
            .iter()
            .find(|&&price| price <= budget && can_resolve(budget - price, &food_trucks[1..]))
            .is_some()
    }
}
