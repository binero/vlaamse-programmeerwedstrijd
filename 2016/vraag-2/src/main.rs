fn main() {
    let input = get_input();
    let lines = &mut input.lines();
    let challange_count: usize = lines.next().unwrap().parse().unwrap();

    for challange_id in 1..(challange_count + 1) {
        let height: usize = lines.next().unwrap().parse().unwrap();
        let width: usize = lines.next().unwrap().parse().unwrap();

        let mut map: Vec<Vec<bool>> = lines
            .take(height)
            .map(|line| line.chars().map(|ch| is_full(ch)).collect())
            .collect();

        let mut sizes = vec![0; width * height + 1];

        for y in 0..height {
            for x in 0..width {
                sizes[read_island_size(x as isize, y as isize, &mut *map)] += 1;
            }
        }

        print!("{}", challange_id);

        for (size, count) in sizes
            .into_iter()
            .enumerate()
            .skip(1)
            .filter(|&(_, count)| count != 0)
        {
            print!(" {} {}", size, count);
        }

        println!();
    }
}

fn get_input() -> String {
    use std::io::Read;
    use std::io::stdin;

    let mut buf = String::new();
    stdin().read_to_string(&mut buf).unwrap();
    buf
}

fn is_full(ch: char) -> bool {
    ch == '+'
}

fn is_filled_at(x: isize, y: isize, map: &[Vec<bool>]) -> bool {
    if x < 0 || y < 0 {
        false
    } else {
        let x = x as usize;
        let y = y as usize;

        if y < map.len() && x < map[y].len() {
            map[y][x]
        } else {
            false
        }
    }
}

fn read_island_size(x: isize, y: isize, map: &mut [Vec<bool>]) -> usize {
    if is_filled_at(x, y, map) {
        map[y as usize][x as usize] = false;
        debug_assert!(!is_filled_at(x, y, map));
        1 + read_island_size(x + 1, y, map) + read_island_size(x - 1, y, map)
            + read_island_size(x, y + 1, map) + read_island_size(x, y - 1, map)
    } else {
        0
    }
}

fn print_map(map: &[Vec<bool>]) {
    for row in map {
        for col in row {
            if *col {
                print!(".");
            } else {
                print!("+");
            }
        }
        println!();
    }
}
