use std::collections::VecDeque;

fn main() {
    use std::io::BufRead;
    use std::io::stdin;

    let input = stdin();
    let lines = &mut input.lock().lines().map(Result::unwrap);

    let challange_count: usize = lines.next().unwrap().parse().unwrap();

    for challange_id in 1..(challange_count + 1) {
        let board_side: usize = lines.next().unwrap().parse().unwrap();
        let board_size = board_side * board_side + 1;
        let mut board = vec![None; board_size];
        let ladder_count: usize = lines.next().unwrap().parse().unwrap();

        for _ in 0..ladder_count {
            let line = lines.next().unwrap();
            let fields = &mut line.split_whitespace();
            let start_field: usize = fields.next().unwrap().parse().unwrap();
            let target_field: usize = fields.next().unwrap().parse().unwrap();
            board[start_field] = Some(target_field);
        }

        println!("{} {}", challange_id, get_shortest(&*board).unwrap());
    }
}

fn get_shortest(map: &[Option<usize>]) -> Option<usize> {
    //debug_assert!(map.len() == visited.len());
    debug_assert!(map.len() > 0);

    let mut todo = VecDeque::new();
    let mut visited = vec![false; map.len()];

    todo.push_back((0, 1));

    while todo.len() > 0 {
        let (current_field, steps) = todo.pop_front().unwrap();
        for target in get_targets(current_field, map, &mut *visited) {
            if target == map.len() - 1 {
                return Some(steps);
            } else {
                todo.push_back((target, steps + 1));
            }
        }
    }

    None
}

fn get_targets(source: usize, map: &[Option<usize>], visited: &mut [bool]) -> Vec<usize> {
    debug_assert!(map.len() == visited.len());
    debug_assert!(source < map.len());

    [
        source + 1,
        source + 2,
        source + 3,
        source + 4,
        source + 5,
        source + 6,
    ].iter()
        .filter_map(|&x| follow(x, map, &mut *visited))
        .collect()
}

fn follow(source: usize, map: &[Option<usize>], visited: &mut [bool]) -> Option<usize> {
    if source >= map.len() || visited[source] {
        None
    } else {
        visited[source] = true;
        if let Some(ladder_target) = map[source] {
            follow(ladder_target, map, visited)
        } else {
            Some(source)
        }
    }
}
