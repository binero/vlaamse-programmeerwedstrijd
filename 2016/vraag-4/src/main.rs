fn main() {
    use std::io::BufRead;
    use std::io::stdin;

    let input = stdin();
    let lines = &mut input.lock().lines().map(Result::unwrap);

    let challange_count: usize = lines.next().unwrap().parse().unwrap();

    for challenge_index in 1..(challange_count + 1) {
        let (height, width) = get_width_height(lines.next().unwrap());

        let mut map = vec![Vec::with_capacity(width); height];
        let mut groups: Vec<Vec<(usize, usize)>> = Vec::new();

        for y in 0..height {
            let line = lines.next().unwrap();
            let numbers = &mut line.split_whitespace();

            for x in 0..width {
                let group_id: usize = numbers.next().unwrap().parse().unwrap();
                let value = numbers.next().unwrap().parse().unwrap();

                map[y].push((group_id, value));

                if group_id >= groups.len() {
                    let extend_size = group_id - groups.len() + 1;
                    groups.extend(std::iter::repeat(Vec::new()).take(extend_size));
                }

                groups[group_id].push((y, x));
            }
        }

        let fail_1 = (0..height)
            .flat_map(move |y| (0..width).map(move |x| (x, y)))
            .filter(|&(x, y)| !test_1_passes(x, y, &*map, &*groups))
            .count();

        let fail_2 = (0..height)
            .flat_map(move |y| (0..width).map(move |x| (x, y)))
            .filter(|&(x, y)| !test_2_passes(x, y, &*map, &*groups))
            .count();

        println!("{} {} {}", challenge_index, fail_1, fail_2);
    }
}

fn test_2_passes(
    x: usize,
    y: usize,
    map: &[Vec<(usize, usize)>],
    groups: &[Vec<(usize, usize)>],
) -> bool {
    let (group_id, value) = map[y][x];

    groups[group_id].len() >= value && value > 0
}

fn test_1_passes(
    x: usize,
    y: usize,
    map: &[Vec<(usize, usize)>],
    groups: &[Vec<(usize, usize)>],
) -> bool {
    let (group_id, value) = map[y][x];
    groups[group_id]
        .iter()
        .filter(|&&(y, x)| {
            let (_, othr_value) = map[y][x];
            othr_value == value
        })
        .count() == 1 && test_neighbour(x, y, map)
}

fn test_neighbour(x: usize, y: usize, map: &[Vec<(usize, usize)>]) -> bool {
    let (_, my_value) = map[y][x];

    let x = x as isize;
    let y = y as isize;

    [
        (x - 1, y),
        (x + 1, y),
        (x, y - 1),
        (x, y + 1),
        (x - 1, y - 1),
        (x + 1, y - 1),
        (x + 1, y + 1),
        (x - 1, y + 1),
    ].iter()
        .cloned()
        .find(|&(x, y)| {
            if x >= 0 && y >= 0 {
                let x = x as usize;
                let y = y as usize;
                y < map.len() && x < map[y].len() && map[y][x].1 == my_value
            } else {
                false
            }
        })
        .is_none()
}

fn get_width_height(line: String) -> (usize, usize) {
    let fields = &mut line.split_whitespace();
    (
        fields.next().unwrap().parse().unwrap(),
        fields.next().unwrap().parse().unwrap(),
    )
}
