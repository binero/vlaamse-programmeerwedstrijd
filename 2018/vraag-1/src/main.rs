fn main() {
    use std::io::BufRead;
    use std::io::stdin;

    let input = stdin();
    let lines = &mut input.lock().lines().map(Result::unwrap);

    let challange_count: usize = lines.next().unwrap().parse().unwrap();

    for challange_index in 1..(challange_count + 1) {
        let line = lines.next().unwrap();
        let mut line = line.split_whitespace()
            .map(|string| string.parse().unwrap());
        let width: usize = line.next().unwrap();
        let height: usize = line.next().unwrap();

        let mut map = vec![vec![0i32; width]; height];

        for y in 0..height {
            let line = lines.next().unwrap();
            let mut line = line.split_whitespace()
                .map(|string| string.parse().unwrap());

            for x in 0..width {
                map[y][x] = line.next().unwrap();
            }
        }

        let (mut cx, mut cy) = get_min(width, height, &mut map);
        let mut output = vec![vec!['.'; width]; height];
        let mut last_character = 0;
        let alphabet = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
            'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        ];

        output[cy][cx] = alphabet[0];

        while let Some((nx, ny)) = get_next_position(cx, cy, &mut map) {
            cx = nx;
            cy = ny;
            last_character += 1;
            output[cy][cx] = alphabet[last_character];
        }

        for row in output {
            let row: String = row.iter().collect();
            println!("{} {}", challange_index, row);
        }
    }
}

fn get_min(width: usize, height: usize, map: &[Vec<i32>]) -> (usize, usize) {
    let coords = (0..height).flat_map(|y| (0..width).map(move |x| (x, y)));

    coords.fold((0, 0), |(ax, ay), (cx, cy)| {
        if map[ay][ax] < map[cy][cx] {
            (ax, ay)
        } else {
            (cx, cy)
        }
    })
}

fn get_next_position(x: usize, y: usize, map: &[Vec<i32>]) -> Option<(usize, usize)> {
    let coords = [(x, y - 1), (x, y + 1), (x - 1, y), (x + 1, y)];
    let current_value = map[y][x];

    let coords_values = coords.into_iter().flat_map(|&(cx, cy)| {
        map.get(cy)
            .and_then(move |row| row.get(cx).map(|value| (cx, cy, value)))
    });

    coords_values
        .fold(None, |acc, (cx, cy, value)| {
            let slope = value - current_value;

            if slope >= 0 {
                if let Some((_ax, _ay, a_slope)) = acc {
                    if a_slope > slope {
                        Some((cx, cy, slope))
                    } else {
                        acc
                    }
                } else {
                    Some((cx, cy, slope))
                }
            } else {
                acc
            }
        })
        .map(|(x, y, _)| (x, y))
}
