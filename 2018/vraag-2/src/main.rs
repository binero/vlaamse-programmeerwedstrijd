fn main() {
    use std::io::BufRead;
    use std::io::stdin;

    let input = stdin();
    let lines = &mut input.lock().lines().map(Result::unwrap);

    let challange_count: usize = lines.next().unwrap().parse().unwrap();

    for challange_index in 1..(challange_count + 1) {
        let line = lines.next().unwrap();
        let mut line = line.split_whitespace()
            .map(|string| string.parse().unwrap());
        let width: usize = line.next().unwrap();
        let height: usize = line.next().unwrap();

        let mut map = vec![vec![' '; width]; height];

        for y in 0..height {
            let line = lines.next().unwrap();
            let mut line = line.chars();
            for x in 0..width {
                map[y][x] = line.next().unwrap();
            }
        }

        let groups = get_groups(width, height, &map);
        let mut cells = vec![vec![0; width]; height];

        for (index, group) in groups.iter().enumerate() {
            for &(x, y) in group {
                cells[y][x] = index;
            }
        }

        let mut results = vec![];

        for (current_group, group) in groups.iter().enumerate() {
            let mut visited = vec![false; groups.len()];
            let mut result = 0;

            for &(cx, cy) in group {
                let neighbours = [(cx, cy - 1), (cx, cy + 1), (cx - 1, cy), (cx + 1, cy)];
                let new_groups: Vec<_> = neighbours
                    .into_iter()
                    .flat_map(|&(cx, cy)| {
                        cells
                            .get(cy)
                            .and_then(move |row| row.get(cx).map(|value| *value))
                    })
                    .filter(|&new_group| new_group != current_group)
                    .collect();

                for group_index in new_groups {
                    if !visited[group_index] {
                        result += 1;
                        visited[group_index] = true;
                    }
                }
            }
            results.push(result);
        }

        for y in 0..height {
            print!("{}", challange_index);
            for x in 0..width {
                let group_index = cells[y][x];
                let neighbours = results[group_index];
                print!(" {}", neighbours);
            }
            println!();
        }
    }
}

fn get_groups(width: usize, height: usize, map: &[Vec<char>]) -> Vec<Vec<(usize, usize)>> {
    let mut visited = vec![vec![false; width]; height];
    let mut results = vec![];

    for y in 0..height {
        for x in 0..width {
            if !visited[y][x] {
                results.push(build_group(x, y, map, &mut visited));
            }
        }
    }

    results
}

fn build_group(
    x: usize,
    y: usize,
    map: &[Vec<char>],
    visited: &mut Vec<Vec<bool>>,
) -> Vec<(usize, usize)> {
    visited[y][x] = true;
    let neighbours = [(x, y - 1), (x, y + 1), (x - 1, y), (x + 1, y)];

    let current_value = map[y][x];

    let coords_values: Vec<_> = neighbours
        .into_iter()
        .flat_map(|&(cx, cy)| {
            map.get(cy)
                .and_then(move |row| row.get(cx).map(|value| (cx, cy, *value)))
        })
        .filter(|&(nx, ny, nv)| nv == current_value && !visited[ny][nx])
        .collect();

    let mut results = vec![(x, y)];
    for (cx, cy, cv) in coords_values {
        results.extend(build_group(cx, cy, map, visited));
    }
    results
}
