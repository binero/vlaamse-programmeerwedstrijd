use std::collections::{HashMap, VecDeque};

fn main() {
    use std::io::BufRead;
    use std::io::stdin;

    let input = stdin();
    let lines = &mut input.lock().lines().map(Result::unwrap);

    let challange_count: usize = lines.next().unwrap().parse().unwrap();

    for challange_index in 1..(challange_count + 1) {
        let input = lines.next().unwrap();
        let input: Vec<bool> = input.chars().map(|ch| ch == '1').collect();
        let code_count: usize = lines.next().unwrap().parse().unwrap();
        let codes = (0..code_count)
            .map(|_| {
                let name = lines.next().unwrap();
                let name = name.chars().next().unwrap();
                let code = lines.next().unwrap();
                let code: Vec<bool> = code.chars().map(|ch| ch == '1').collect();
                (name, code)
            })
            .collect();

        println!("{} {}", challange_index, solve(input, codes));
    }
}

fn solve(input: Vec<bool>, mut codes: Vec<(char, Vec<bool>)>) -> String {
    let mut answers: Vec<String> = Vec::new();
    let mut answer_length = None;
    let mut work: VecDeque<(&[bool], String)> = VecDeque::new();
    codes.sort_by_key(|&(name, _)| name);
    codes.sort_by_key(|&(_name, ref vector)| vector.len());
    work.push_back((&input, "".into()));

    while let Some((input, so_far)) = work.pop_front() {
        if input.len() == 0 {
            if let Some(ref mut answer_length) = answer_length {
                if so_far.len() == *answer_length {
                    answers.push(so_far.clone());
                }
            } else {
                answer_length = Some(so_far.len());
                answers.push(so_far.clone());
            }
        }

        if let Some(answer_length) = answer_length {
            if so_far.len() > answer_length {
                break;
            }
        }

        let valid_codes = codes
            .iter()
            .take_while(|&&(code_name, ref code)| code.len() <= input.len())
            .filter(|&&(code_name, ref code)| code == &&input[0..code.len()])
            .map(|&(code_name, ref code)| {
                let mut so_far = so_far.clone();
                so_far.push(code_name);
                (&input[code.len()..], so_far)
            });

        work.extend(valid_codes)
    }

    if answers.len() > 0 {
        answers.sort();
        answers[0].clone()
    } else {
        "ONMOGELIJK".into()
    }
}
