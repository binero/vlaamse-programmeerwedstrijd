fn main() {
    use std::io::BufRead;
    use std::io::stdin;

    let input = stdin();
    let lines = &mut input.lock().lines().map(Result::unwrap);

    let alphabet = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    ];

    let challange_count: usize = lines.next().unwrap().parse().unwrap();

    for challange_index in 1..(challange_count + 1) {
        let one = lines.next().unwrap();
        let two = lines.next().unwrap();

        let mut one: Vec<Thing> = one.chars().map(Into::into).collect();
        let mut two: Vec<Thing> = two.chars().map(Into::into).collect();

        let mut differences: Vec<_> = make_up(&one, &two);

        let mut permutations = vec![];
        loop {
            let index_uno: Option<usize> = differences.first().cloned();
            if let Some(index_uno) = index_uno {
                let index_duo = differences
                    .iter()
                    .cloned()
                    .find(|&index_duo| one[index_uno] == two[index_duo]);

                if let Some(index_duo) = index_duo {
                    if index_uno != index_duo {
                        one.swap(index_uno, index_duo);
                        permutations
                            .push(format!("{}{}", alphabet[index_uno], alphabet[index_duo]));
                        differences = make_up(&one, &two);
                    }
                } else {
                    permutations.clear();
                    permutations.push("onmogelijk".into());
                    break;
                }
            } else {
                break;
            }
        }

        permutations.sort();
        let result: String = permutations
            .iter()
            .flat_map(|string| string.chars())
            .collect();

        if result.len() == 0 {
            println!("{} correct", challange_index);
        } else {
            println!("{} {}", challange_index, result);
        }
    }
}

#[derive(Eq, PartialEq, Clone, Copy)]
enum Thing {
    S,
    R,
    L,
}

impl From<char> for Thing {
    fn from(ch: char) -> Self {
        match ch {
            'S' => Thing::S,
            'R' => Thing::R,
            'L' => Thing::L,
            _ => panic!(),
        }
    }
}

fn make_up(one: &[Thing], two: &[Thing]) -> Vec<usize> {
    one.iter()
        .enumerate()
        .zip(two.iter())
        .filter(|&((index, one), two)| one != two)
        .map(|((index, one), two)| index)
        .collect()
}
