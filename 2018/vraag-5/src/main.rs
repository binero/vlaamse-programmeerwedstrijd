fn main() {
    use std::io::BufRead;
    use std::io::stdin;

    let input = stdin();
    let lines = &mut input.lock().lines().map(Result::unwrap);

    let challange_count: usize = lines.next().unwrap().parse().unwrap();
}
