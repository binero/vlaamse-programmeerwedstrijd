use std::io::BufRead;

fn call<I: Iterator<Item = String>>(index: usize, input_iter: &mut I) -> Result<(), Box<Error>> {
    let numbers: Vec<usize> = input_iter.map(|x| x.parse().unwrap()).collect();

    println!(
        "{} {} {}",
        index,
        numbers.iter().min().expect("No min"),
        numbers.iter().max().expect("No max"),
    );

    Ok(())
}

// Don't look down

fn main() {
    if let Err(error) = run() {
        eprintln!("{}", error);
    }
}

use std::error::Error;
fn run() -> Result<(), Box<Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int(&mut input).expect("No ex. count");

    for excercise_index in 1..(excercise_count + 1) {
        let line_count: usize =
            read_int(&mut input).expect(&format!("No line count for {}", excercise_count));

        let mut excercise_iter = (&mut input).take(line_count);
        call(excercise_index, &mut excercise_iter)
            .unwrap_or_else(|err| eprintln!("Ex {} failed: {}", excercise_index, err));

        {
            let count = excercise_iter.count();
            if count > 0 {
                eprintln!("Test case {} failed: {} lines left", excercise_index, count);
            }
        }
    }
    Ok(())
}

fn read_int<I: Iterator<Item = String>>(input: &mut I) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
