use std::io::BufRead;

fn call(index: usize, input_iter: &mut impl Iterator<Item = String>) -> Result<(), Box<dyn Error>> {
    let numbers: Vec<usize> = input_iter.map(|x| x.parse().unwrap()).collect();

    println!(
        "{} {} {}",
        index,
        numbers.iter().min().expect("No min"),
        numbers.iter().max().expect("No max"),
    );

    Ok(())
}

// Don't look down

use std::error::Error;
fn main() -> Result<(), Box<dyn Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int(&mut input).expect("No ex. count");

    for excercise_index in 1..=excercise_count {
        let line_count: usize =
            read_int(&mut input).expect(&format!("No line count for {}", excercise_count));

        let mut excercise_iter = (&mut input).take(line_count);
        call(excercise_index, &mut excercise_iter)
            .unwrap_or_else(|err| eprintln!("Ex {} failed: {}", excercise_index, err));

        {
            let count = excercise_iter.count();
            if count > 0 {
                eprintln!("Test case {} failed: {} lines left", excercise_index, count);
            }
        }
    }
    Ok(())
}

fn read_int(input: &mut impl Iterator<Item = String>) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
