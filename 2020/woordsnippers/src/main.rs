use std::io::BufRead;

fn call<I: Iterator<Item = String>>(index: usize, input_iter: &mut I) -> Result<(), Box<Error>> {
    let numbers: Vec<usize> = input_iter.map(|x| x.parse().unwrap()).collect();

    println!(
        "{} {} {}",
        index,
        numbers.iter().min().expect("No min"),
        numbers.iter().max().expect("No max"),
    );

    Ok(())
}

// Don't look down

fn main() {
    if let Err(error) = run() {
        eprintln!("{}", error);
    }
}

use std::error::Error;
fn run() -> Result<(), Box<Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int(&mut input).expect("No ex. count");

    for excercise_index in 1..(excercise_count + 1) {
        let line = input.next().unwrap();
        let substrings: Vec<_> = line.split_whitespace().collect();

        let mut cache = Vec::new();
        complete(&mut cache, &substrings);
        println!("{}", build_string(&cache, &substrings));
    }
    Ok(())
}

fn build_string(cache: &[usize], substrings: &[&str]) -> String {
    if cache.len() > 0 {
        cache
            .iter()
            .map(|&index| substrings[index].chars().nth(0).unwrap())
            .chain(substrings[*cache.last().unwrap()].chars().skip(1))
            .collect()
    } else {
        String::new()
    }
}

fn complete(cache: &mut Vec<usize>, substrings: &[&str]) -> bool {
    let k = substrings[0].len();
    if (build_string(cache, substrings).len() == substrings.len() + k - 1) {
        return true;
    }

    for index in (0..substrings.len()) {
        if !cache.contains(&index)
            && (cache.len() == 0
                || substrings[cache.last().cloned().unwrap()]
                    .chars()
                    .skip(1)
                    .take(k - 1)
                    .eq(substrings[index].chars().take(k - 1)))
        {
            cache.push(index);
            if !complete(cache, substrings) {
                cache.pop();
            } else {
                return true;
            }
        } else {
            continue;
        }
    }
    return false;
}

fn read_int<I: Iterator<Item = String>>(input: &mut I) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
