use std::io::BufRead;

#[derive(Clone, Copy)]
struct Street {
    pub start_idx: usize,
    pub end_idx: usize,
    pub len: usize,
    pub required: bool,
}

#[derive(Debug)]
struct QueueItem {
    pub current_crossing_idx: usize,
    pub current_len: usize,
    pub required_street_idxs: Vec<usize>,
    pub step_len: usize,
    pub visited_streets_once: Vec<usize>,
    pub visited_streets_twice: Vec<usize>,
}

fn call(
    index: usize,
    input_iter: &mut impl Iterator<Item = String>,
    crossing_count: usize,
    street_count: usize,
) -> Result<(), Box<dyn Error>> {
    let mut required_street_idxs = vec![];
    let mut street_lookup: Vec<Vec<usize>> = vec![vec![]; crossing_count + 1];
    let streets: Vec<Street> = input_iter
        .take(street_count)
        .enumerate()
        .map(|(idx, x)| {
            let ws: Vec<usize> = x
                .split_whitespace()
                .map(|item| item.parse().unwrap())
                .collect();
            if ws[3] == 1 {
                required_street_idxs.push(idx);
            }
            street_lookup[ws[0]].push(idx);
            street_lookup[ws[1]].push(idx);
            Street {
                start_idx: ws[0],
                end_idx: ws[1],
                len: ws[2],
                required: ws[3] == 1,
            }
        })
        .collect();

    let start_idx: usize = input_iter.next().unwrap().parse().unwrap();
    let mut queue = vec![QueueItem {
        current_crossing_idx: start_idx,
        current_len: 0,
        required_street_idxs: required_street_idxs.clone(),
        step_len: 0,
        visited_streets_once: vec![],
        visited_streets_twice: vec![],
    }];

    for _ in 0..5000 {
        queue.sort_unstable_by_key(|item| item.current_len);

        let next = queue.swap_remove(0);
        if next.required_street_idxs.is_empty() && next.current_crossing_idx == start_idx {
            println!("{} {}", index, next.current_len);
            return Ok(());
        }

        street_lookup[next.current_crossing_idx]
            .iter()
            .map(|&street_idx| (street_idx, streets[street_idx]))
            .filter(|(street_idx, _)| !next.visited_streets_twice.contains(street_idx))
            .for_each(|(street_idx, item)| {
                let mut once = next.visited_streets_once.clone();
                let mut twice = next.visited_streets_twice.clone();

                if next.visited_streets_once.contains(&street_idx) {
                    twice.push(street_idx);
                } else {
                    once.push(street_idx);
                }

                let next_crossing_idx = if item.start_idx == next.current_crossing_idx {
                    item.end_idx
                } else {
                    item.start_idx
                };

                queue.push(QueueItem {
                    current_crossing_idx: next_crossing_idx,
                    current_len: item.len + next.current_len,
                    required_street_idxs: next
                        .required_street_idxs
                        .iter()
                        .filter(|idxx| **idxx != street_idx)
                        .cloned()
                        .collect(),
                    step_len: next.step_len + 1,
                    visited_streets_once: once,
                    visited_streets_twice: twice,
                })
            });
    }

    Ok(())
}

// Don't look down

use std::error::Error;
fn main() -> Result<(), Box<dyn Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int_string(&mut input).expect("No ex. count");

    for excercise_index in 1..=excercise_count {
        let ws = input.next().unwrap();
        let mut input_number = ws.split_whitespace().into_iter();

        let street_count = read_int(&mut input_number).unwrap();
        let crossing_count = read_int(&mut input_number).unwrap();

        let mut excercise_iter = (&mut input).take(street_count + 1);
        call(
            excercise_index,
            &mut excercise_iter,
            crossing_count,
            street_count,
        )
        .unwrap_or_else(|err| eprintln!("Ex {} failed: {}", excercise_index, err));

        {
            let count = excercise_iter.count();
            if count > 0 {
                eprintln!("Test case {} failed: {} lines left", excercise_index, count);
            }
        }
    }
    Ok(())
}

fn read_int<'a>(input: &mut impl Iterator<Item = &'a str>) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}

fn read_int_string<'a>(input: &mut impl Iterator<Item = String>) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
