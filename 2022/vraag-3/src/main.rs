use std::io::BufRead;

use std::collections::HashMap;
use std::iter::repeat;

struct CacheVal {
    pub idx: usize,
    pub amount: usize,
}

fn call(index: usize, input_iter: &mut String) -> Result<(), Box<dyn Error>> {
    let numbers: Vec<isize> = input_iter
        .split_whitespace()
        .map(|x| x.parse().unwrap())
        .collect();
    if numbers.len() == 0 {
        println!("{}", index);
        return Ok(());
    }

    // let highers: Vec<usize> = numbers
    //     .clone()
    //     .into_iter()
    //     .enumerate()
    //     .map(|(idx, cur)| numbers.iter().skip(idx).filter(|item| **item < cur).count())
    //     .collect();
    let mut cache: HashMap<isize, usize> = HashMap::new();
    //let mut cache: Vec<usize> = vec![0; 1000];
    let min = numbers.iter().min().unwrap();
    let highers = numbers.iter().enumerate().rev().map(|(_, &current)| {
        let mut sum = 0;
        for lower_count in (*min - 1)..=current {
            sum = sum + *cache.entry(lower_count).or_default();
        }
        *cache.entry(current).or_insert(0) += 1;
        sum
    });

    let highers_str: Vec<String> = highers
        .into_iter()
        .rev()
        .map(|higher| format!("{}", higher))
        .collect::<Vec<String>>();

    let highers_str = highers_str.join(" ");
    if highers_str.len() == 0 {
        println!("{}", index);
    } else {
        println!("{} {}", index, highers_str);
    }
    Ok(())
}

// Don't look down

use std::error::Error;
fn main() -> Result<(), Box<dyn Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int(&mut input).expect("No ex. count");

    for excercise_index in 1..=excercise_count {
        let _array_len = input.next().unwrap();

        call(excercise_index, &mut input.next().unwrap())
            .unwrap_or_else(|err| eprintln!("Ex {} failed: {}", excercise_index, err));
    }
    Ok(())
}

fn read_int(input: &mut impl Iterator<Item = String>) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
