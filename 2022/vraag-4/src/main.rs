use std::io::BufRead;

fn call(index: usize, input_iter: String) -> Result<(), Box<dyn Error>> {
    let mut input = input_iter
        .split_whitespace()
        .map(|item| item.parse().unwrap());

    let w: isize = input.next().unwrap();
    let o = input.next().unwrap();
    let hw = input.next().unwrap();
    let ho = input.next().unwrap();

    let mut middle = (w + o) / 2;
    let stream_amount: isize = if w > middle {
        (w - middle.max(hw).max(ho)).max(0)
    } else {
        -(o - middle.max(ho).max(hw)).max(0)
    };
    //println!("   SA {}", stream_amount);

    let new_w = w - stream_amount;
    let new_o = o + stream_amount;

    if new_w == new_o {
        println!("{} gelijk", index);
    } else {
        println!("{} {} {}", index, new_w, new_o);
    }

    Ok(())
}

// Don't look down

use std::error::Error;
fn main() -> Result<(), Box<dyn Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int(&mut input).expect("No ex. count");

    for excercise_index in 1..=excercise_count {
        let mut excercise_iter = (&mut input).next().unwrap();
        call(excercise_index, excercise_iter)
            .unwrap_or_else(|err| eprintln!("Ex {} failed: {}", excercise_index, err));
    }
    Ok(())
}

fn read_int(input: &mut impl Iterator<Item = String>) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
