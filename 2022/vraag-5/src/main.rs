use std::io::BufRead;

struct Player {
    pub move_smaller: usize,
    pub move_bigger: usize,
}
impl Player {
    fn new(a: usize, b: usize) -> Self {
        if a < b {
            Self {
                move_smaller: a,
                move_bigger: b,
            }
        } else {
            Self {
                move_smaller: b,
                move_bigger: a,
            }
        }
    }
}

fn solve(
    current_player: usize,
    current_coins: usize,
    players: Vec<Player>,
    solutions: &mut Vec<usize>,
    winning: Vec<usize>,
    winner: &mut Option<usize>,
) -> (Vec<usize>, Option<usize>) {
    //let old_current_coins = current_coins;

    // Small
    {
        let new_current = current_coins + players[current_player].move_smaller;
        if winning.contains(&new_current) {
            solutions.push(new_current);
            *winner = Some(current_player);
        } else if new_current > *winning.iter().max().unwrap() {
        } else {
            return solve(
                current_player + 1 % players.len(),
                new_current,
                players,
                solutions,
                winning,
                winner,
            );
        }
    }
    // Bigger
    {
        let new_current = current_coins + players[current_player].move_bigger;
        if winning.contains(&new_current) {
            solutions.push(new_current);
            *winner = Some(current_player);
        } else if new_current > *winning.iter().max().unwrap() {
        } else {
            return solve(
                current_player + 1 % players.len(),
                new_current,
                players,
                solutions,
                winning,
                winner,
            );
        }
    }

    return (solutions.clone(), *winner);
}

fn call(index: usize, input_iter: &mut impl Iterator<Item = String>) -> Result<(), Box<dyn Error>> {
    let numbers: Vec<usize> = input_iter.map(|x| x.parse().unwrap()).collect();

    let _ = input_iter.next().unwrap();
    let finish_numbers: Vec<usize> = input_iter
        .next()
        .unwrap()
        .split_whitespace()
        .map(|item| item.parse().unwrap())
        .collect();

    let mut input = input_iter.next().unwrap();
    let mut input = input.split_whitespace().map(|item| item.parse().unwrap());

    let current: usize = input.next().unwrap();
    let possible_a1 = input.next().unwrap();
    let possible_a2 = input.next().unwrap();
    let possible_b1 = input.next().unwrap();
    let possible_b2 = input.next().unwrap();
    let output = solve(
        0,
        current,
        vec![
            Player::new(possible_a1, possible_a2),
            Player::new(possible_b1, possible_b2),
        ],
        &mut vec![],
        finish_numbers,
        &mut None,
    );
    let out = format!(
        "{} {} {}",
        index,
        match output.1 {
            Some(0) => "win",
            Some(1) => "verlies",
            _ => "gelijk",
        },
        output
            .0
            .iter()
            .map(|item| format!("{}", item))
            .collect::<Vec<String>>()
            .join(" ")
    );
    println!("{}", out.trim());

    Ok(())
}

// Don't look down

use std::error::Error;
fn main() -> Result<(), Box<dyn Error>> {
    let stdin = std::io::stdin();
    let mut input = stdin.lock().lines().map(Result::unwrap);

    let excercise_count: usize = read_int(&mut input).expect("No ex. count");

    for excercise_index in 1..=excercise_count {
        let mut excercise_iter = (&mut input).take(3);
        call(excercise_index, &mut excercise_iter)
            .unwrap_or_else(|err| eprintln!("Ex {} failed: {}", excercise_index, err));

        {
            let count = excercise_iter.count();
            if count > 0 {
                eprintln!("Test case {} failed: {} lines left", excercise_index, count);
            }
        }
    }
    Ok(())
}

fn read_int(input: &mut impl Iterator<Item = String>) -> Option<usize> {
    input.next().and_then(|x| x.parse().ok())
}
